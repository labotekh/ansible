#!/bin/sh
whoami
pwd
chmod -v 700 $(pwd)
export ANSIBLE_HOST_KEY_CHECKING=False # disable host key checking for ansible
# setup ansible-vault and SSH key
mkdir secret # create secret dir
echo "$ANSIBLE_SSH_KEY" > secret/ansible.key # create private key from gitlab variable
echo "$ANSIBLE_VAULT_PASSWORD" > secret/ansible_vault_password
chmod -R 400 secret

git submodule deinit --force group_vars
sed -i "s/https:\/\/gitlab.com\/labotekh\/iac\/group_vars.git/https:\/\/imotekh:$CI_ACCESS_VARS_TOKEN@gitlab.com\/labotekh\/iac\/group_vars.git/g" .gitmodules
git submodule init group_vars
git submodule update --init

# install requirements
ansible-galaxy collection install -r requirements.yml
