#!/bin/sh
#ansible-galaxy collection install labotekh_fr.iac
LOCAL_VERSION=$(cat galaxy.yml | grep version: | cut -d ' ' -f 2)
REMOTE_VERSION=$(ansible-galaxy collection list | grep labotekh_fr.iac | cut -d ' ' -f 2)
if [ "$LOCAL_VERSION" != "$REMOTE_VERSION" ];
then
	ansible-galaxy collection build
    ansible-galaxy collection publish labotekh_fr-iac-* --token $ANSIBLE_COLLECTION_TOKEN
fi
